let newElement = document.createElement('div');
      let newText = document.createTextNode("Kata 1");
      newElement.appendChild(newText);
      let destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata1() {
        for(i=1; i<=25; i++) {
            if (i === 1) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i > 1 && i < 25) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
        }
    }
        kata1()
        
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 2");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata2() {
      for(i=25; i>=1; i--) {
          if (i === 25) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i < 25 && i > 1) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata2()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 3");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata3() {
      for(i=-1; i>=-25; i--) {
          if (i === -1) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i < -1 && i > -25) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata3()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 4");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata4() {
      for(i=-25; i<=-1; i++) {
          if (i === -25) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i > -25 && i < -1) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata4()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 5");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata5() {
      for(i=25; i>=-25; i-=2) {
          if (i === 25) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i < 25 && i > -25) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata5()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 6");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata6() {
      for(i=3; i<=100; i++) {
          if (i === 3) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i % 3 === 0 && i !== 99) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else if (i === 99) {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata6()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 7");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata7() {
      for(i=7; i<=100; i++) {
          if (i === 7) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i % 7 === 0 && i !== 98) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else if (i === 98) {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata7()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 8");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata8() {
      for(i=100; i>=3; i--) {
          if (i === 99) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i !== 3 && i % 3 === 0 || i % 7 === 0) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else if (i === 3) {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata8()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 9");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata9() {
      for(i=5; i<=100; i++) {
          if (i === 5) {
              newElement=document.createElement('div')
              newText = document.createTextNode(i + ", ");
              newElement.appendChild(newText);
              destination = document.getElementById('d1');
              destination.appendChild(newElement);
          } else if (i % 5 === 0 && i % 2 !== 0 && i !== 95) {
            newText = document.createTextNode(i + ", ");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          } else if (i === 95) {
            newText = document.createTextNode(i + ".");
            newElement.appendChild(newText);
            destination = document.getElementById('d1');
            destination.appendChild(newElement);
          }
      }
    }
      kata9()
      
      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 10");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      
      const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];
      
      newElement = document.createElement('div')
      destination = document.getElementById('d1')
      destination.appendChild(newElement)
      function kata10() {
      for (let i=0; i<sampleArray.length; i++ ) {
        newText = document.createTextNode(sampleArray[i] + ", ")
        newElement.appendChild(newText)
        destination = document.getElementById('d1')
        destination.appendChild(newElement)
      }
    }
      kata10()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 11");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div')
      destination = document.getElementById('d1')
      destination.appendChild(newElement)
      function kata11() {
      for (let i=0; i<sampleArray.length; i++ ) {
        if (sampleArray[i] % 2 === 0) {
        newText = document.createTextNode(sampleArray[i] + ", ")
        newElement.appendChild(newText)
        destination = document.getElementById('d1')
        destination.appendChild(newElement)
        }
      }
    }
      kata11()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 12");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div')
      destination = document.getElementById('d1')
      destination.appendChild(newElement)
      function kata12() {
      for (let i=0; i<sampleArray.length; i++ ) {
        if (sampleArray[i] % 2 !== 0) {
        newText = document.createTextNode(sampleArray[i] + ", ")
        newElement.appendChild(newText)
        destination = document.getElementById('d1')
        destination.appendChild(newElement)
        }
      }
    }
      kata12()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 13");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div')
      destination = document.getElementById('d1')
      destination.appendChild(newElement)
      function kata13() {
      for (let i=0; i<sampleArray.length; i++ ) {
        if (sampleArray[i] % 8 === 0) {
        newText = document.createTextNode(sampleArray[i] + ", ")
        newElement.appendChild(newText)
        destination = document.getElementById('d1')
        destination.appendChild(newElement)
        }
      }
    }
      kata13()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 14");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div')
      destination = document.getElementById('d1')
      destination.appendChild(newElement)
      function kata14() {
      for (let i=0; i<sampleArray.length; i++ ) {
        newText = document.createTextNode(sampleArray[i]*sampleArray[i] + ", ")
        newElement.appendChild(newText)
        destination = document.getElementById('d1')
        destination.appendChild(newElement)
      }
    }
      kata14()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 15");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata15() {
      let sum = 0
      let number = 1
      while (number <= 20) {
        sum += number
        number++
      }
      newText = document.createTextNode(sum);
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
    }
      kata15()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 16");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata16() {
      sum = 0
      for (let i = 0; i < sampleArray.length; i++) {
        sum += sampleArray[i]
      }
      newText = document.createTextNode(sum);
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
    }
      kata16()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 17");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata17() {
      newText = document.createTextNode(Math.min(...sampleArray));
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      }
      kata17()

      newElement = document.createElement('br');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      newElement = document.createElement('div');
      newText = document.createTextNode("Kata 18");
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);

      newElement = document.createElement('div');
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      function kata18() {
      newText = document.createTextNode(Math.max(...sampleArray));
      newElement.appendChild(newText);
      destination = document.getElementById('d1');
      destination.appendChild(newElement);
      }
      kata18()